var express = require('express');
var Redis = require('ioredis');
var promisify = require('util').promisify;

var app = express();
var parser = express.urlencoded({extended: true});
var redis = new Redis({
    port:14140,
    host:"redis-14140.c135.eu-central-1-1.ec2.cloud.redislabs.com",
    password:"G3VJq6AL1jLAe7iP8qbaNfaFpWa2C4Zk",
    db:0
});

app.get('/', async (req, res) => {
    const name = await promisify(redis.get).bind(redis)('name');
    const bills = await promisify(getAllBills)().catch((err) => console.log(err));
    const mappedBills = bills.map(element => `<li><a href="/getBill?billId=${element.id}">${element.description}</li>`).sort().reduce((accumulator, currentValue) => accumulator += currentValue, "");
    res.send(`
    <h1>${name}</h1><br />
    <a href='/newbill'>Nowy rachunek</a><br />
    <ul> ${mappedBills} </ul>
    `);
});

app.get('/getbill', async (req, res) => {
    try {
    const billId = req.query.billId;
    const billDetails = await promisify(getBillDetails)(billId);
    const billElements = await promisify(getBillElements)(billId);
    const products = await promisify(getProducts)()
    const mappedBillProducts = billElements.map(element => `<li>${element.name} ${element.price} <a href="/removefrombill?billId=${billId}&productId=${element.id}">Usuń</a></li>`)
        .reduce((accumulator, currentValue) => accumulator += currentValue, "");
    const productsOptions = products.map(element => `<option value="${element.id}">${element.category}: ${element.name} - ${element.price} zł`).sort()
        .reduce((accumulator, currentValue) => accumulator += currentValue, "");
    const sum = billElements.map(element => parseFloat(element.price)).reduce((accumulator, currentValue) => accumulator += currentValue, 0)
    res.send(`
    <h1>${billDetails.description}</h1>
    <form action="/">
    <input type="submit" value="Powrót" />
    </form>
    <form method="post" action="addToBill">
    <input type="hidden" id="billId" name="billId" value="${billId}">
    <select name="product">${productsOptions}</select>
    <input type="submit" value="Dodaj" />
    </form>
    <ul> ${mappedBillProducts} </ul>
    Suma: ${sum} zł<br />
    <a href="/closebill?billId=${billId}">Zamknij rachunek</a>
    `);
    } catch(err) {
        console.log(err);
        res.sendStatus(500);
    }
});

app.post('/addToBill', parser, async(req, res) => {
    try {
        console.log(req.body.billId + " " + req.body.product);
        await promisify(addToBill)(req.body.billId, req.body.product);
        res.redirect(req.get('referer'));
    } catch(err) {
        console.log(err);
        res.sendStatus(500);
    }
})

app.get('/newbill', (req, res) => {
    res.send(`
    <h1>Nowy rachunek</h1><br>
    <form method="post" action="newbill">
      Opis:<br />
      <input type="text" name="desc" /><br />
      <br />
      <input type="submit" value="Utwórz" />
    </form>
    `)
});

app.post('/newbill', parser, async (req, res) => {
    try {
        const billDetails = await promisify(createNewBill)(req.body.desc);
        res.redirect(`/getbill?billId=bill:${billDetails.id}`);
        console.log(req.body.desc);
    } catch(err) {
        console.log(err);
        res.sendStatus(500);
    }
});

app.get('/removefrombill', async (req, res) => {
    try {
        await promisify(removeFromBill)(req.query.billId, req.query.productId);
        res.redirect(req.get('referer'));
    } catch(err) {
        console.log(err);
        res.sendStatus(500);
    }
})

app.get('/closebill', async (req, res) => {
    try {
        await promisify(closeBill)(req.query.billId);
        res.redirect(`/`);
    } catch(err) {
        console.log(err);
        res.sendStatus(500);
    }
})


function getBillsKeys(callback) {
    try {
        var pipeline = redis.pipeline();
        redis.keys('bill:*', function(err, klucze) {
            for (var i = 0; i < klucze.length; i++) {
                pipeline.lrange(klucze[i], 0, -1);
            }
            pipeline.exec(function(err,result){
                var bills = result.map(element => { return element[1] })
                callback(null, bills);
            });
        });   
    } catch(err) {
        callback(err, null);
    }
}

function getAllBills(callback) {
    try {
        var pipeline = redis.pipeline();
        redis.keys('bill:*', function(err, klucze) {
            for (var i = 0; i < klucze.length; i++) {
                pipeline.hgetall(klucze[i]);
            }
            pipeline.exec(function(err,result){
                var bills = [];
                for (var i = 0; i < klucze.length; i++) {
                    console.log(result[i][1]);
                    if(result[i][1].isOpen === "true")
                        bills.push({ id: klucze[i], description: result[i][1].desc});
                }
                callback(null, bills);
            });
        });   
    } catch(err) {
        callback(err, null);
    }
}

function getBillElementsKeys(billId, callback) {
    try {
        redis.hgetall(billId, (err, result) => {
            redis.lrange(result.elements, 0, -1, (err, elements) => {
                callback(null, elements);
            })
        })     
    } catch(err) {
        callback(err, null);
    }
}

async function getBillDetails(billId, callback) {
    try {
        redis.hgetall(billId, (err, element) => {
            callback(null, {id: billId, description: element.desc, isOpen: element.isOpen})
        });        
    } catch(err) {
        callback(err, null);
    }
}

async function getBillElements(billId, callback) {
    try {
        var pipeline = redis.pipeline();
        var elementKeys = await promisify(getBillElementsKeys)(billId);
        for (var i = 0; i < elementKeys.length; i++) {
            pipeline.hgetall(elementKeys[i]);
        }
        pipeline.exec(function(err,element){
            var products = [];
            for (var i = 0; i < elementKeys.length; i++) {
                products.push({id: elementKeys[i], name: element[i][1].name, price: element[i][1].price, category: ""});
            }
            callback(null, products); 
        });
        
    } catch(err) {
        callback(err, null);
    }
}

async function createNewBill(desc, callback) {
    try {
        var billId = await promisify(redis.incr).bind(redis)('bill');
        var billElementsId = await promisify(redis.incr).bind(redis)('billelements');
        redis.hmset('bill:' + billId, ["desc", desc, "elements",`billelements:${billElementsId}`, "isOpen", true]);
        callback(null, {id: billId, description: desc});
    } catch(err) {
        callback(err, null);
    }
}

async function getProducts(callback) {
    try {
        var keys = await promisify(redis.keys).bind(redis)('product:*');
        var pipeline = redis.pipeline();
        keys.forEach(key => pipeline.hgetall(key))
        pipeline.exec(function(err,result){
            var products = Promise.all(result.map(async (element, index) => {
                categoryObject = await promisify(redis.get).bind(redis)(element[1].category);
                return Promise.resolve({ id: keys[index], name: element[1].name, price: element[1].price, category: categoryObject })
            }))
            callback(null, products);
        });         
    } catch(err) {
        callback(err, null);
    }
}

async function addToBill(billId, productId, callback) {
    try {
        const billObject = await promisify(redis.hgetall).bind(redis)(billId);
        redis.rpush(billObject.elements, productId);
        callback(null, null);
    } catch(err) {
        callback(err, null);
    }
}

async function removeFromBill(billId, productId, callback) {
    try {
        const billObject = await promisify(redis.hgetall).bind(redis)(billId);
        console.log(billObject);
        promisify(redis.lrem).bind(redis)(billObject.elements, 1, productId);
        callback(null, null);
    } catch(err) {
        callback(err, null);
    }
}

async function closeBill(billId, callback) {
    try {
        promisify(redis.hmset).bind(redis)(billId, ["isOpen", false]);
        callback(null, null);
    } catch(err) {
        callback(err, null);
    }
}

app.listen(3000, function () {
    console.log('Library app listening on port 3000!');
  });